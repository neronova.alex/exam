$(document).ready(function (){
    $('.close-create-comment-modal').click(() => {
        $("#create-comment").trigger('reset');
    });

    $('#create-comment-btn').click(function (event) {
        event.preventDefault();

        const data = $('#create-comment').serialize();
        const articleId = $('#article_id').val();

        $.ajax({
            url: `/articles/${articleId}/comments`,
            method: "POST",
            data: data
        })
            .done(function (response) {
                renderData(response.comment);
            }).fail(function(response) {
            console.log(response);
        });
    });

    function renderData(comment) {
        let commentsBlock = $('.scrollit');
        $(commentsBlock).append(comment);
        clearForm();
    }

    function clearForm() {
        $("#create-comment").trigger('reset');
    }
});
