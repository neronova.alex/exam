@extends('layouts.app')

@section('content')
<div class="container">
    @auth()
        <div class="m-1">
            <a class="btn btn-outline-primary mb-2" href="{{route('articles.create')}}">Create news</a>
        </div>
    @endauth
    <h1>News:</h1>
    @if($articles->count() > 0)
        <div class="table-responsive overflow-scroll">
            <table class="table table-striped table-hover table-sm">
                <thead>
                <tr>
                    <th scope="col">News</th>
                    <th scope="col">Tags</th>
                    <th scope="col">Category</th>
{{--                    <th scope="col">Published date</th>--}}
                    <th scope="col">Created date</th>
                    <th scope="col">Author</th>
                </tr>
                </thead>
                <tbody>
                @foreach($articles as $article)
                    <tr class="align-middle">
                        <td>
                            <a href="{{route('articles.show',['article' => $article])}}">{{$article->body}}</a></td>
                        <td>
                            @if($article->tags->isNotEmpty())
                                    @foreach($article->tags as $tag)
                                        <div class="badge border border-info btn-outline-info">
                                            <p>{{$tag->name}}</p>
                                        </div>
                                    @endforeach
                            @endif
                        </td>

                        <td>{{$article->category->name}}</td>
{{--                        <td>--}}
{{--                               {{$article->published_at->format('d M Y - H:i')}}--}}
{{--                        </td>--}}
                        <td>{{$article->created_at->format('Y-m-d')}}</td>
                        <td>{{$article->user->name}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="row justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $articles->links('pagination::bootstrap-4') }}
            </div>
        </div>

    @else
        <p>Нет новостей</p>
    @endif
</div>
@endsection

