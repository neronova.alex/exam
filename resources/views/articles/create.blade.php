@extends('layouts.app')

@section('content')

<div class="container">
    <h3 class="page-title">Create news</h3>
    <form method="post" action="{{ route('articles.store') }}">
        @csrf
        <div class="form-group mb-2">
                    <label for="body"><b>Text</b></label>
                    <input type="text" class="form-control" id="body" name="body">
                    @error('body')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                    <label for="category_id"><b>Category</b></label>
                    <select class="form-control border-info  @error('category_id') is-invalid border-danger @enderror"
                            id="category_id"  name="category_id">
                        <option selected >Choose category</option>
                        @foreach(App\Models\Category::all() as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>

                <label for="tags" class="form-label fw-bold mt-1"><b>Tags</b></label>
                <p>
                    <a class="btn btn-primary" data-toggle="collapse" href="#tags" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Tags
                    </a>
                </p>

                <div class="collapse" id="tags">
                    <div class="card card-body">
                        @foreach($tags as $tag)
                            <div class="form-check">
                                <input class="form-check-input" name="tags[]" type="checkbox"
                                value="{{$tag->id}}" id="tags{{$tag->id}}">
                                <label class="form-check-label" for="tags{{$tag->id}}">
                                    {{$tag->name}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
        </div>
        <div class="my-3">
             <button type="submit" class="btn btn-primary">Create article</button>
        </div>
    </form>

</div>

@endsection
