@extends('layouts.app')

@section('content')

<div class="container">
    <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-8">
                <div class="card-body">
                    <p class="card-text">{{$article->body}}</p>
                    <p class="card-text"><b>Category:</b> {{$article->category->name}}</p>
                    @if($tags->isNotEmpty())
                        <div class="text mt-3">
                            <strong>Tags:</strong>
                        </div>
                        @foreach($tags as $tag)
                            <div class="badge border border-info text-uppercase btn-outline-info">
                                <p>{{$tag->name}}</p>
                            </div>
                        @endforeach
                    @endif
                    <p class="card-text"><b>Author:</b> {{$article->user->name}}</p>
                    <p class="card-text"><small class="text-muted"><b>Published at:</b> {{$article->published_at->format('d M Y - H:i')}}</small></p>
                    <p class="card-text"><small class="text-muted"><b>Created at:</b> {{$article->created_at->format('d M Y - H:i')}}</small></p>
                </div>
            </div>
        </div>
    </div>

    @auth()
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Comments
                    </button>
                </div>

                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-8 scrollit">
                                <input type="hidden" id="article_id" value="{{$article->id}}">
                                @foreach($article->comments as $comment)
                                    <div id="comment-{{$comment->id}}" class="">

                                        <div class="media g-mb-30 media-comment">
                                            <div class="media-body u-shadow-v18 g-bg-secondary g-pa-30">
                                                <div class="g-mb-15">
                                                    <h5 class="h5 g-color-gray-dark-v1 mb-0">{{$comment->user->name}}</h5>
                                                    <div id="form-comment-{{$comment->id}}"></div>
                                                    <span class="g-color-gray-dark-v4 g-font-size-12">{{$comment->created_at->diffForHumans()}}</span>
                                                </div>
                                                <p id="comment-{{$comment->id}}-body">
                                                    {{$comment->content}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-4 fixed">
                <div class="comment-form">
                    <form id="create-comment">
                        @csrf
                        <input type="hidden" id="article_id" value="{{$article->id}}">
                        <div class="form-group">
                            <label for="bodyId">Comment</label>
                            <textarea id="bodyId" name="content" class="form-control" rows="3"
                                      required></textarea>
                        </div>
                        <button id="create-comment-btn" type="submit" class="btn btn-outline-primary btn-sm btn-block">Add new
                            comment
                        </button>
                    </form>
                </div>
            </div>
    @endauth

    <a href="{{route('articles.index')}}">Назад ко всем новостям</a>

</div>
@endsection
