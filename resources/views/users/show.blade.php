@extends('layouts.app')

@section('content')
   <div class="container">
       <div class="row g-0">
           <div class="col-md-8">
               <div class="card-body">
                   <h5 class="card-title">
                       <strong>Имя:</strong> {{$user->name}}
                   </h5>
                   <div>
                       <strong>E-Mail:</strong> {{$user->email}}
                   </div>
                   <div>
                       <strong>Дата Регистрации:</strong> {{$user->created_at}}
                   </div>
                   @if($user->is_admin)
                       <div>
                           <strong>Статус:</strong>
                           @if($user->is_admin)
                               <div class="badge bg-danger text-dark custom-width">
                                   Администратор
                               </div>
                           @endif
                       </div>
                   @endif

               </div>
           </div>
       </div>
   </div>

@endsection
