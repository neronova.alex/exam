## Контрольная работа

### Нужна авторизация и регистрация пользователей.
Функционал реализован.

### Нужна возможность публиковать новости как администраторам системы, так и рядовым пользователям.
Функционал реализован.
Реализовано на главной странице.

### Новости должны:
- Иметь тело в виде текста +
- Помечаться тэгами +
- Определяться в категорию +
- Обладать датой публикации и датой создания - и то и другое должно отображаться при просмотре новости
- Иметь автора, который тоже должен отображаться при просмотре новости. +
 
Функционал реализован частично. Трудности возникли с датой публикации.

### Рядовой пользователь может увидеть новость на сайте, если только ей указана дата публикации и дата публикации уже наступила. Рядовой пользователь не должен иметь возможности указать дату публикации.
Функционал реализован не полностью.

### Администратор системы должен иметь возможность модерировать и назначать дату публикации новостям.
Функционал не реализован.

### Нужно сделать функционал просмотра новостей списком с постраничностью и фильтрацией по категориям и тегам. Также должна быть страница детального просмотра новости.
Функционал реализован частично. Фильтрация не реализована.
Реализован на главной странице.

### Должен быть функционал комментирования новостей и модерирования комментариев администратором
Функционал реализован частично.

### Каждая новость пользователем может быть оценена по трем критериям:
- Качественно / некачественно - оценка качества подачи и полноты  информации в новости
- Актуальна / не актуальна - оценка актуальности, злободневности новости
- Доволен / не доволен - оценка своего отношения к обстоятельству, описанному новостью.
 
Функционал не реализован.

### У всех, кто публикует новости на сайте, должен быть рейтинг рассчитываемый по формуле:  (Ar + Q + A) / 3, где:
Ar - количество опубликованных пользователем новостей
Q - суммарное качество новостей (может быть отрицательным)
A - суммарная актуальность новостей (может быть отрицательной)

Функционал не реализован.

### Должна быть страница профайла пользователя. На странице должна быть информация о рейтинге пользователя и его регистрационные данные (кроме пароля, конечно).
Функционал реализован частично. Рейтинг не реализован.
При нажатии на имя пользователя есть доступ к профайлу.
