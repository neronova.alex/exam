<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentsController extends Controller
{

    /**
     * @param Article $article
     * @param CommentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Article $article, CommentRequest $request)
    {
        $comment = new Comment();
        $comment->content = $request->input('content');
        $comment->user()->associate($request->user());
        $comment->article_id = $article->id;
        $comment->save();
        return response()->json([
            'comment' => view('comments.comment', compact('comment'))->render()], 201);
    }
}
