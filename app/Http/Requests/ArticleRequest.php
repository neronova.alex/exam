<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'body' => 'bail|required|max:2048|min:3|string',
            'user_id' => 'exists:App\Models\User,id',
            'published_at' => 'date',
            'is_approved' => 'boolean'
        ];
    }
}
