<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(TagTableSeeder::class);
        $this->call(ArticleTableSeeder::class);

        $articles = Article::all();
        $tags = Tag::all();
        $users = User::all();

        for ($i = 0; $i < 10; $i++) {
            $tags->each(function ($tag) use ($articles) {
                $tag->articles()->attach(
                    $articles->random(1)->pluck('id')->toArray()
                );
            });
        }

        for ($i = 0; $i < 50; $i++) {
            Comment::factory(rand(0, 3))
                ->state([
                    'user_id' => $users->random(),
                    'article_id' => $articles->random()
                ])
                ->create();
        }
    }
}
