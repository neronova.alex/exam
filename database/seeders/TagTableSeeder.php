<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag = [
            [
                'name' => 'international relations'
            ],
            [
                'name' => 'breaking news'
            ],
            [
                'name' => 'young people'
            ],
            [
                'name' => 'policy solution'
            ],
        ];

        foreach ($tag as $key => $value) {
            Tag::create($value);
        }
    }
}
