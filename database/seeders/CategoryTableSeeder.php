<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = [
            [
                'name' => 'Health'
            ],
            [
                'name' => 'Travel'
            ],
            [
                'name' => 'Politics'
            ],
            [
                'name' => 'Economics'
            ],
        ];

        foreach ($category as $key => $value) {
            Category::create($value);
        }
    }
}
