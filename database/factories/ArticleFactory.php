<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'body' => $this->faker->paragraph($nbSentences = 20),
            'category_id' => rand(1, 4),
            'published_at' => $this->faker->date(),
            'user_id' => rand(1, 9)
        ];
    }
}
